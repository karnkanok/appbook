import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import BookLists from '../screen/BookLists';
import Home from '../screen/Home';
import AddBook from '../screen/AddBook';
import MyBook from '../screen/MyBook';

const Stack = createStackNavigator();
export default () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={() => {
          return {
            headerShown: false,
          };
        }}
      />
      <Stack.Screen name="BookLists" component={BookLists} />
      <Stack.Screen name="AddBook" component={AddBook} />
      <Stack.Screen name="MyBook" component={MyBook} />
    </Stack.Navigator>
  </NavigationContainer>
);
