import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export default function Add({navigation}) {
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={() => navigation.navigate('AddBook')}>
      <View style={styles.input}>
        <Text style={styles.textTitle}>Add Book</Text>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  touchable: {
    backgroundColor: 'rgb(203,209,215)',
    padding: '5%',
    flexDirection: 'row',
  },
  input: {
    width: '90%',
    borderBottomWidth: 1,
    borderColor: ' #7f7f7f',
    paddingBottom: 7,
    marginTop: 7,
    backgroundColor: 'transparent',
  },
  textBt: {
    padding: '5%',
    marginLeft: '5%',
    backgroundColor: '#7f7f7f',
  },
  textTitle: {
    paddingLeft: 7,
  },
});
//  width: 100%;
//   border-bottom-width: 1px;
//   border-color: #7f7f7f;
//   padding-left: 10px;
//   padding-bottom: 7px;
//   margin-top: 15px;
//   background-color: transparent;
