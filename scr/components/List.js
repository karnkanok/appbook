import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Col, Row, Grid} from 'react-native-easy-grid';

export default function List({data, navigation}) {
  const TextDeteal = ({data, fontSize, fontWeight}) => {
    return (
      <Text
        numberOfLines={1}
        style={{
          fontSize: fontSize || 14,
          color: 'rgb(58,46,34)',
          fontWeight: fontWeight || null,
        }}>
        {data}
      </Text>
    );
  };
  // MyBook
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.component}
      onPress={() => navigation.navigate('MyBook', {data: {item}})}>
      <View>
        <Image
          style={styles.image}
          source={{
            uri: 'https://i.pinimg.com/564x/0d/37/4b/0d374b74cc2beed1bcf25b8a1a2e8e01.jpg',
          }}
        />
        <View style={styles.textTime}>
          <TextDeteal data={item.date} />
        </View>
      </View>
      <TextDeteal data={item.title} fontSize={16} fontWeight={'bold'} />
      <TextDeteal data={item.author_name} />
      <TextDeteal data={item.finish_reading_date} />
    </TouchableOpacity>
  );
  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        numColumns={'3'}
        renderItem={renderItem}
        style={{backgroundColor: 'rgb(142,182,168)'}}
        keyExtractor={item => item.id}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  component: {
    margin: '1.1%',
    backgroundColor: 'rgb(183,211,200)',
    width: '31%',
    borderRadius: 5,
    padding: 5,
  },
  image: {
    width: '100%',
    height: 150,
    marginTop: '2%',
  },
  textTime: {
    backgroundColor: 'rgba(209,150,104,0.4)',
    position: 'absolute',
    borderRadius: 5,
    padding: 3,
    top: 10,
    left: 10,
  },
  container: {
    flex: 1,
  },
});
