import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';

export default function Details({data}) {
  console.log(data.item.description);
  return (
    <View style={styles.container}>
      <View style={{alignItems: 'center'}}>
        <Image
          style={styles.image}
          source={{
            uri: 'https://i.pinimg.com/564x/0d/37/4b/0d374b74cc2beed1bcf25b8a1a2e8e01.jpg',
          }}
        />
        <View style={styles.header}>
          <Text style={styles.headerText}>{data.item.title}</Text>
          <Text style={styles.titleText}>{data.item.author_name}</Text>
        </View>
      </View>
      <View style={styles.title}>
        <View style={styles.subTitle}>
          <Text style={styles.subTitleText}>
            {data.item.finish_reading_date}
          </Text>
          <Text style={styles.textColor}>วันที่อ่านจบ</Text>
        </View>
        <View style={styles.subTitle}>
          <Text style={styles.subTitleText}>{data.item.date}</Text>
          <Text style={styles.textColor}>เปิดให้อ่าน</Text>
        </View>
      </View>

      <View style={styles.description}>
        <Text style={styles.textColor}>{data.item.description}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(142,182,168)',
  },
  image: {
    width: '50%',
    height: 150,
    marginTop: '2%',
  },
  header: {
    marginTop: '3%',
  },
  title: {
    flexDirection: 'row',
    alignContent: 'space-around',
    backgroundColor: '#c3cb',
    marginTop: '3%',
  },
  subTitle: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'rgb(183,211,200)',
    paddingVertical: '1%',
  },
  description: {
    padding: '5%',
  },
  headerText: {fontWeight: 'bold', fontSize: 20, color: 'rgb(58,46,34)'},
  titleText: {fontWeight: 'bold', fontSize: 18, color: 'rgb(107,99,107)'},
  subTitleText: {fontWeight: '700', fontSize: 16, color: '#000'},
  textColor: {fontWeight: '500', fontSize: 16, color: '#000'},
});
