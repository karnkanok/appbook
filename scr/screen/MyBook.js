import React from 'react';
import {View, ScrollView} from 'react-native';
import Details from '../components/Details';

export default function MyBook({navigation, route}) {
  const Data = route.params.data;
  return (
    <>
      <Details data={Data} />
    </>
  );
}
