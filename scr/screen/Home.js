import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Button,
  ImageBackground,
} from 'react-native';
import List from '../components/List';
// import firebase from '../../database/firebase';
import BookLists from './BookLists';
import Add from '../components/Add';

export default function Home({navigation}) {
  return (
    <>
      <Add />
      <BookLists navigation={navigation} />
    </>
    // <ImageBackground
    //   source={{
    //     uri: 'https://i.pinimg.com/564x/c7/09/c5/c709c503cc537d89541176816b5bf916.jpg',
    //   }}
    //   resizeMode="stretch"
    //   style={styles.imageStyle}>
    //   <View style={styles.contents}>
    //     <Button
    //       title="Go Book Lists"
    //       // color="#f194ff"
    //       color="rgb(221,178,142)"
    //       style={styles.buttonStyle}
    //       onPress={() => navigation.navigate('BookLists')}
    //     />
    //   </View>
    // </ImageBackground>
  );
}

const styles = StyleSheet.create({
  contents: {
    alignItems: 'center',
    marginTop: '90%',
    width: '100%',
  },
  imageStyle: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgb(222,202,179)',
    flex: 1,
  },
  textStyle: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '900',
    color: '#411',
    shadowColor: '#f777',
  },
  buttonStyle: {
    width: '100%',
    height: '40%',
    alignItems: 'center',
  },
});
