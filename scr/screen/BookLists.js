import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
// import firebase from '../../database/firebase';
import List from '../components/List';
// import {getBook, addBook} from '../api/BookApi';

export default function BookLists({navigation}) {
  // const dataO = firebase.firestore().collection('Books');
  // const ref = firestore().collection('Books');
  // const [bookList, setBookList] = useState([]);
  // console.log('ref', dataO);

  const data = [
    {
      id: '1',
      title: '1234155555',
      date: '10/10/65',
      finish_reading_date: '10/10/64',
      image: 'https://thestandard.co/wp-content/uploads/2020/03/1-50.jpg',
      author_name: 'testttttttttttttttt',
      description:
        'หลีกหนีความจริงไปสัมผัสช่วงเวลาดีๆ ของผู้เขียนและครอบครัวที่ขับรถบ้านออกเที่ยวรอบนิวซีแลนด์ ‘Home Sweet Motorhome’ จะเล่าถึงช่วงเวลาที่บ้านจะกลายเป็นบ้าน การเที่ยวด้วยรถบ้านคือการรวมเอาทั้งสองอย่างเข้าด้วยกันได้อย่างลงตัวที่สุด สนุกสนานที่สุด วุ่นวายที่สุด และน่าจดจำเป็นที่สุด บทสนทนาระหว่างครอบครัว การปรุงอาหาร และสถานที่ท่องเที่ยวแสนสวยที่จะทำให้คุณมีความสุขจนลืมโลกแห่งความจริงอันแสนโหดร้ายไปได้เลย',
    },
    {
      id: '2',
      title: '1234155555',
      date: '10/10/65',
      finish_reading_date: '10/10/64',
      author_name: 'testttttttttttttttt',
      image: 'https://thestandard.co/wp-content/uploads/2020/03/1-50.jpg',
      description:
        'หลีกหนีความจริงไปสัมผัสช่วงเวลาดีๆ ของผู้เขียนและครอบครัวที่ขับรถบ้านออกเที่ยวรอบนิวซีแลนด์ ‘Home Sweet Motorhome’ จะเล่าถึงช่วงเวลาที่บ้านจะกลายเป็นบ้าน การเที่ยวด้วยรถบ้านคือการรวมเอาทั้งสองอย่างเข้าด้วยกันได้อย่างลงตัวที่สุด สนุกสนานที่สุด วุ่นวายที่สุด และน่าจดจำเป็นที่สุด บทสนทนาระหว่างครอบครัว การปรุงอาหาร และสถานที่ท่องเที่ยวแสนสวยที่จะทำให้คุณมีความสุขจนลืมโลกแห่งความจริงอันแสนโหดร้ายไปได้เลย',
    },
    {
      id: '3',
      title: '1234155555',
      date: '10/10/65',
      finish_reading_date: '10/10/64',
      author_name: 'testttttttttttttttt',
      image: 'https://thestandard.co/wp-content/uploads/2020/03/1-50.jpg',
      description:
        'หลีกหนีความจริงไปสัมผัสช่วงเวลาดีๆ ของผู้เขียนและครอบครัวที่ขับรถบ้านออกเที่ยวรอบนิวซีแลนด์ ‘Home Sweet Motorhome’ จะเล่าถึงช่วงเวลาที่บ้านจะกลายเป็นบ้าน การเที่ยวด้วยรถบ้านคือการรวมเอาทั้งสองอย่างเข้าด้วยกันได้อย่างลงตัวที่สุด สนุกสนานที่สุด วุ่นวายที่สุด และน่าจดจำเป็นที่สุด บทสนทนาระหว่างครอบครัว การปรุงอาหาร และสถานที่ท่องเที่ยวแสนสวยที่จะทำให้คุณมีความสุขจนลืมโลกแห่งความจริงอันแสนโหดร้ายไปได้เลย',
    },
    {
      id: '4',
      title: '1234155555',
      date: '10/10/65',
      finish_reading_date: '10/10/64',
      author_name: 'testttttttttttttttt',
      image: 'https://thestandard.co/wp-content/uploads/2020/03/1-50.jpg',
      description:
        'หลีกหนีความจริงไปสัมผัสช่วงเวลาดีๆ ของผู้เขียนและครอบครัวที่ขับรถบ้านออกเที่ยวรอบนิวซีแลนด์ ‘Home Sweet Motorhome’ จะเล่าถึงช่วงเวลาที่บ้านจะกลายเป็นบ้าน การเที่ยวด้วยรถบ้านคือการรวมเอาทั้งสองอย่างเข้าด้วยกันได้อย่างลงตัวที่สุด สนุกสนานที่สุด วุ่นวายที่สุด และน่าจดจำเป็นที่สุด บทสนทนาระหว่างครอบครัว การปรุงอาหาร และสถานที่ท่องเที่ยวแสนสวยที่จะทำให้คุณมีความสุขจนลืมโลกแห่งความจริงอันแสนโหดร้ายไปได้เลย',
    },
    {
      id: '5',
      title: '1234155555',
      date: '10/10/65',
      finish_reading_date: '10/10/64',
      author_name: 'testttttttttttttttt',
      image: 'https://thestandard.co/wp-content/uploads/2020/03/1-50.jpg',
      description:
        'หลีกหนีความจริงไปสัมผัสช่วงเวลาดีๆ ของผู้เขียนและครอบครัวที่ขับรถบ้านออกเที่ยวรอบนิวซีแลนด์ ‘Home Sweet Motorhome’ จะเล่าถึงช่วงเวลาที่บ้านจะกลายเป็นบ้าน การเที่ยวด้วยรถบ้านคือการรวมเอาทั้งสองอย่างเข้าด้วยกันได้อย่างลงตัวที่สุด สนุกสนานที่สุด วุ่นวายที่สุด และน่าจดจำเป็นที่สุด บทสนทนาระหว่างครอบครัว การปรุงอาหาร และสถานที่ท่องเที่ยวแสนสวยที่จะทำให้คุณมีความสุขจนลืมโลกแห่งความจริงอันแสนโหดร้ายไปได้เลย',
    },
  ];
  // const onBooksReceived = bookList => {
  //   console.log(bookList);
  //   setBookLists(prev => ({bookList: (prev.bookList = bookList)}));
  // };
  // useEffect(() => {
  //   getBook(onBooksReceived);
  // });
  // console.log('bookList', bookList);

  return (
    <View style={{flex: 1}}>
      <List data={data} navigation={navigation} />
    </View>
  );
}
