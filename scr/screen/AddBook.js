import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Button} from 'react-native';
import Input from '../components/Input';
// import DatePicker from 'react-native-date-picker';
// import {getBook, addBook} from '../api/BookApi';
// import firebase from '../../database/firebase';

export default function AddBook({navigation}) {
  const [form, setForm] = useState({
    title: '',
    name: '',
    url: '',
    date: '',
    finish_reading_date: '',
  });
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  // const dbRef = firebase.firestore().collection('Books');

  const handleInput = name => text => {
    setForm({
      ...form,
      [name]: text,
    });
  };
  const onPressAdd = () => {
    // dbRef
    //   .add({
    //     title: form.title,
    //     name: form.name,
    //     url: form.url,
    //     date: form.date,
    //     finish_reading_date: form.finish_reading_date,
    //   })
    //   .then(res => {
    //     setForm({
    //       title: '',
    //       name: '',
    //       url: '',
    //       date: '',
    //       finish_reading_date: '',
    //     });
    //     navigation.navigate('Home');
    //   })
    //   .catch(err => {
    //     console.log('Error : ', err);
    //   });
    // navigation.navigate('Home');
    // addBook({
    //   title: form.title,
    //   name: form.name,
    //   url: form.url,
    //   date: form.date,
    //   finish_reading_date: form.finish_reading_date,
    // });
    console.log('form', form);
  };
  return (
    <View style={styles.contaner}>
      <Input
        styles={styles.inputStyle}
        placeholder="ชื่อหนังสือ"
        placeholderTextColor="#000"
        value={form.title}
        onChangeText={handleInput('title')}
      />
      <Input
        styles={styles.inputStyle}
        placeholder="ชื่อผู้แต่ง"
        placeholderTextColor="#000"
        value={form.name}
        onChangeText={handleInput('name')}
      />
      <Input
        styles={styles.inputStyle}
        placeholder="URL รูปภาพ"
        placeholderTextColor="#000"
        value={form.url}
        onChangeText={handleInput('url')}
      />
      {/* <Button title="Open" onPress={() => setOpen(true)} />
      <DatePicker
        modal="date"
        open={open}
        date={date}
        onConfirm={date => {
          setOpen(false);
          setDate(date);
        }}
        onCancel={() => {
          setOpen(false);
        }}
      /> */}
      <Input
        styles={styles.inputStyle}
        placeholder="วันที่อ่านจบ"
        placeholderTextColor="#000"
        value={form.date}
        // secureTextEntry={typePassword}
        onChangeText={handleInput('date')}
      />
      <Input
        styles={styles.inputStyle}
        placeholder="ระยะเวลาที่ให้อ่าน"
        placeholderTextColor="#000"
        value={form.finish_reading_date}
        // secureTextEntry={typePassword}
        onChangeText={handleInput('finish_reading_date')}
      />
      <TouchableOpacity
        style={styles.touchable}
        onPress={() => navigation.navigate('Home')}>
        <Text style={styles.textBt}>Add</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  contaner: {
    flex: 1,
    padding: 15,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: 'rgb(203,209,215)',
  },
  inputStyle: {
    // backgroundColor: 'rgb(203,209,215)',
    // borderRadius: 15,
    borderBottomWidth: 1,
    borderColor: ' #7f7f7f',
    paddingLeft: 17,
    color: '#000',
    fontSize: 14,
    height: 50,
  },
  textBt: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    color: 'rgb(58,46,34)',
  },
  touchable: {
    marginTop: 20,
    backgroundColor: 'rgb(142,182,168)',
    borderRadius: 15,
    width: '50%',
    padding: '3%',
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
